.. index:: Geometry Nodes; Redistribute Curve Points

*************************
Redistribute Curve Points
*************************

Redistributes existing control points evenly along each curve.

.. peertube:: syFssvhthKJUo45B2dxw5W


Inputs
======

**Curves**

Factor
   Factor to blend overall effect.

Feature Awareness
   Use simple feature awareness to keep feature definition.


Properties
==========

This node has no properties.


Outputs
=======

**Curves**
